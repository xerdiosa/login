from django.test import TestCase, Client
from django.urls import resolve

from time import sleep

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class UnitTest(TestCase):
    def test_if_url_exist(self):
        response = Client().get("/")
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'homepage/index.html')
    
    def test_404(self):
        response = Client().get("/bogus-page")
        self.assertEqual(response.status_code, 404)

# class functional_test(StaticLiveServerTestCase):
#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         options  = Options()
#         options.add_argument('--dns-prefetch-disable')
#         options.add_argument('--no-sandbox')
#         options.add_argument('disable-gpu')
#         options.add_argument('--disable-dev-shm-usage')
#         options.headless = True
#         try:
#             cls.selenium = webdriver.Chrome(options=options)
#         except:
#             cls.selenium = webdriver.Chrome('./chromedriver', options=options)
#         super(functional_test, cls).setUpClass()
#         cls.selenium.implicitly_wait(10)

#     @classmethod
#     def tearDownClass(cls):
#         cls.selenium.quit()
#         super().tearDownClass()

#     def test_create_account_login_and_logout(self):
#         self.selenium.get(self.live_server_url)

#         self.assertIn('You are not logged in :(', self.selenium.page_source)

#         button = self.selenium.find_element_by_id('loginButton')
#         button.click()

#         sleep(3)

#         button = self.selenium.find_element_by_id('create-account-button')
#         button.click()

#         input_name = self.selenium.find_element_by_id('id_username')
#         input_name.send_keys('bogususername')

#         input_pass = self.selenium.find_element_by_id('id_password1')
#         input_pass.send_keys('confirmedpassword')

#         input_confirm_pass = self.selenium.find_element_by_id('id_password2')
#         input_confirm_pass.send_keys('confirmedpassword')

#         button = self.selenium.find_element_by_id('create-account-button')
#         button.click()       

#         input_name = self.selenium.find_element_by_id('id_username')
#         input_name.send_keys('bogususername')

#         input_pass = self.selenium.find_element_by_id('id_password')
#         input_pass.send_keys('confirmedpassword')

#         button = self.selenium.find_element_by_id('login-button')
#         button.click()

#         sleep(3)

#         self.assertIn('hi bogususername, have a nice day!', self.selenium.page_source)



