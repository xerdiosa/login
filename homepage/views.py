from django.shortcuts import render

def index(request):
    response = {'user':request.user}
    return render(request, "homepage/index.html", response)