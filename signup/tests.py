from django.test import TestCase, Client
from django.urls import resolve

# Create your tests here.
class UnitTest(TestCase):
    def test_if_login_url_exist(self):
        response = Client().get("/accounts/login")
        self.assertEqual(response.status_code, 301)

    def test_if_signup_url_exist(self):
        response = Client().get("/accounts/signup")
        self.assertEqual(response.status_code, 301)